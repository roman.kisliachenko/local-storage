# local-storage

A tiny library containing `@Stored` property wrapper for persisting data across application launches. By default, UserDefaults is used under the hood, but it's configurable.


## Example

### UserDefaults
```swift
enum Localization: Codable {
    case en, th, ru, uk
        
    @Stored(key: "language", default: .en)
    static var current: Self
}
```

### Keychain
First of all, the dependency needs to be added:
```swift
.package(url: "https://github.com/kishikawakatsumi/KeychainAccess.git", from: "3.0.0")
```
Some keychain settings:

```swift
import KeychainAccess

let keychain = Bundle.main.bundleIdentifier.map(Keychain.init(service:))!

extension Keychain: StorageProtocol {
    public func getData(for key: String) throws -> Data? {
        try getData(key)
    }
    public func set(_ data: Data, for key: String) throws {
        try set(data, key: key)
    }
}
```
Finally, `@Stored` can be used as before, but the keychain storage needs to be provided explicitly:
```swift
enum Localization: Codable {
    case en, th, ru, uk
        
    @Stored(storage: keychain, key: "language", default: .en)
    static var current1: Self
}
```

## Additional capabilities
In the case of a decoding process failure (while retrieving the data from a storage), a migration can be configured. Here's an example of migration for situations where the data used to always be saved in the first cell of an array (a real-life production legacy code situation, by the way 🤯). For this, we only need to override the `func migrate(on error: Error, with data: Data) throws -> Value?` from the `Migratable` protocol (which `Stored` already conforms to):
```swift
extension Stored {
    func migrate(on error: Error, with data: Data) throws -> Value? {
        guard case DecodingError.typeMismatch = error else { return nil }
        print("Will try to migrate")
        guard let value = try JSONDecoder().decode([Value].self, from: data).first else { 
            return nil 
        }
        print("Migrating...")
        runCatch(set, with: value)
        return value
    }
}
```
Well... the README becomes longer than the source code itself.  
If there are still questions, perhaps it's best to just take a look at the source code 😉

## Requirements

Xcode 15 / Swift 5.9 / MacOS 10.15 / iOS 13

## Dependencies

This library relies on a single dependency:  
[`DoCatching`](https://gitlab.com/roman.kisliachenko/do-catching) - a lightweight library: a set of wrappers over `do-try-catch` syntax.

## Installation
Just add this package to your project as a dependency via Swift Package Manager. For further details, please see below.  
In the case your project is an iOS application:  
XCode -> File -> Add Package Dependencies... -> Search for "local-storage" -> Add Package  
In the case your project is a package add these lines to your Package.swift file:
```swift
// ...
dependencies: [
    .package(url: "https://gitlab.com/roman.kisliachenko/local-storage", from: "1.0.0")
],
// ...
``` 

Remember to `import LocalStorage` at the top of those files, where you're planning to use the functionality.

## License

Please refer to [`LICENSE.txt`](LICENSE.txt) for more information.  
TL;DR: This project is licensed under the Apache License 2.0
