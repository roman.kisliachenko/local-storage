import Foundation
import DoCatching

public protocol DataStorage: AnyObject {
    func getData(for: String) throws -> Data?
    func set(_: Data, for: String) throws
}

@propertyWrapper
public struct Stored<Storage: DataStorage, Value: Codable>: Migratable {
    private let storage: Storage
    private let key: String
    private let `default`: Value
    
    public init(storage: Storage = UserDefaults.standard, key: String, default: Value) {
        self.storage = storage
        self.key = key
        self.default = `default`
    }
    
    public lazy var wrappedValue = flatCatch(storage.getData, with: key).flatMap(getValue) ?? `default` {
        didSet { runCatch(set, with: wrappedValue) }
    }
    
    private func getValue(from data: Data) -> Value? {
        getCatch(JSONDecoder().decode, with: Value.self, data, onError: handle)
    }
    
    private func handle(error: Error, _: Value.Type, with data: Data) -> Value? {
        flatCatch(migrate, with: error, data)
    }
    
    public func set(_ value: Value) throws {
        let data = try JSONEncoder().encode(value)
        try storage.set(data, for: key)
    }
}

// MARK: - UserDefaults storage by default

extension UserDefaults: DataStorage {
    public func getData(for key: String) -> Data? {
        data(forKey: key)
    }
    
    public func set(_ data: Data, for key: String) {
        set(data, forKey: key)
    }
}

// MARK: - Migratable

public protocol Migratable {
    associatedtype Value: Codable
    func migrate(on error: Error, with data: Data) throws -> Value?
    func set(_ value: Value) throws
}

public extension Migratable {
    func migrate(on error: Error, with data: Data) -> Value? {
        nil
    }
}
