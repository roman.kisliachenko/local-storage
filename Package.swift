// swift-tools-version: 5.9

import PackageDescription

let package = Package(
    name: "local-storage",
    platforms: [.macOS(.v10_15), .iOS(.v13)],
    products: [
        .library(name: "LocalStorage", targets: ["LocalStorage"])
    ],
    dependencies: [
        .package(url: "https://gitlab.com/roman.kisliachenko/do-catching", from: "1.0.0")
    ],
    targets: [
        .target(name: "LocalStorage", dependencies: [
            .product(name: "DoCatching", package: "do-catching")
        ]),
        // Test Targets
        .testTarget(name: "LocalStorageTests", dependencies: ["LocalStorage"])
    ]
)
