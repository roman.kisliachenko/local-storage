import XCTest
import LocalStorage

final class LocalizationTests: XCTestCase {
    enum Localization: Codable {
        case en, th, ru, uk
        
        @Stored(key: "language1", default: .en)
        static var current1: Self
        
        @Stored(key: "language2", default: .en)
        static var current2: Self
        
        @Stored(key: "language3", default: .en)
        static var current3: Self
        
        @Stored(key: "language4", default: .en)
        static var current4: Self
        
        @Stored(key: "language5", default: .en)
        static var current5: Self?
        
        @Stored(key: "language6", default: .en)
        static var current6: Self?
    }
    
    // MARK: - Localization tests
    
    func testDefaultValueInitialization() {
        UserDefaults.standard.removeObject(forKey: "language1")
        XCTAssertEqual(Localization.current1, .en)
    }
    
    func testStoredValueInitialization() {
        UserDefaults.standard.set(try! JSONEncoder().encode(Localization.th), forKey: "language2")
        XCTAssertEqual(Localization.current2, .th)
    }
    
    func testSetValue() {
        UserDefaults.standard.removeObject(forKey: "language3")
        Localization.current3 = .ru
        let data = UserDefaults.standard.data(forKey: "language3")!
        let language = try! JSONDecoder().decode(Localization.self, from: data)
        XCTAssertEqual(language, .ru)
    }
    
    // MARK: - Test Nil Behavior
    
    func testStoredNullDefaultValueInitialization() {
        UserDefaults.standard.set(Data("null".utf8), forKey: "language4")
        XCTAssertEqual(Localization.current4, .en)
    }
    
    func testStoredNullNilValueInitialization() {
        UserDefaults.standard.set(Data("null".utf8), forKey: "language5")
        XCTAssertNil(Localization.current5)
    }
    
    func testSetNil() {
        UserDefaults.standard.removeObject(forKey: "language6")
        Localization.current6 = nil
        let data = UserDefaults.standard.data(forKey: "language6")!
        let language = try! JSONDecoder().decode(Localization?.self, from: data)
        XCTAssertNil(language)
    }
}
