import XCTest
import LocalStorage

final class UserSettingsTests: XCTestCase {
    struct UserSettings {
        @Stored(key: "fontSize", default: 14)
        var fontSize: Int
        
        @Stored(key: "darkMode", default: true)
        var darkMode: Bool?
    }
    
    var settings = UserSettings()
    
    // MARK: - setUp
    
    override func setUp() {
        super.setUp()
        UserDefaults.standard.removeObject(forKey: "fontSize")
        UserDefaults.standard.removeObject(forKey: "darkMode")
    }
    
    // MARK: - UserSettings tests
    
    func testInitialUserSettings() {
        XCTAssertEqual(settings.fontSize, 14)
        XCTAssertTrue(settings.darkMode!)
    }
    
    func testChangeUserSettings() {
        settings.fontSize = 16
        settings.darkMode = false
        XCTAssertEqual(settings.fontSize, 16)
        XCTAssertFalse(settings.darkMode!)
    }
    
    func testUserSettingsPersistence() {
        settings.fontSize = 18
        settings.darkMode = false
        settings = UserSettings() // application relaunch simulation
        XCTAssertEqual(settings.fontSize, 18)
        XCTAssertFalse(settings.darkMode!)
    }
    
    func testNilBehavior() {
        UserDefaults.standard.set(Data("null".utf8), forKey: "fontSize")
        settings.darkMode = nil
        settings = UserSettings() // application relaunch simulation
        XCTAssertEqual(settings.fontSize, 14)
        XCTAssertNil(settings.darkMode)
    }
}
